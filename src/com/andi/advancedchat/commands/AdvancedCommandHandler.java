package com.andi.advancedchat.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.andi.advancedchat.AdvancedChat;
import com.andi.advancedchat.cell.CellHandler;
import com.andi.advancedchat.listeners.AdvancedListener;

public class AdvancedCommandHandler implements CommandExecutor {

	private AdvancedChat plugin;
	private CellHandler cellH;

	public AdvancedCommandHandler(AdvancedChat plugin) {
		this.plugin = plugin;
		this.cellH = plugin.getCellHandler();

		plugin.getCommand("phone").setExecutor(this);
		plugin.getCommand("global").setExecutor(this);
		plugin.getCommand("l").setExecutor(this);
		plugin.getCommand("spy").setExecutor(this);
		plugin.getCommand("ac").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String Label, String[] args) {

		if (!(sender instanceof Player))
			return true;

		Player ply = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("phone")) {
			if (args.length == 0) {
				ply.sendMessage(ChatColor.BLUE + "Use: /phone block [player] - To block them from calling you");
				ply.sendMessage(ChatColor.BLUE + "Use: /phone unblock [player] - To unblock a player. So they can call you.");
				return true;
			}
			if (args[0].equalsIgnoreCase("block")) {
				if (args.length == 1) {
					ply.sendMessage(ChatColor.RED + "Usage: /phone block [Player] - To block them from calling you");
				} else {
					Player targetPlayer = Bukkit.getPlayer(args[1]);
					if (targetPlayer == null || !targetPlayer.isOnline()) {
						ply.sendMessage(ChatColor.RED + args[1] + " is either offline or does not exist!");
						return true;
					}
					String targetName = targetPlayer.getName();
					cellH.addBlock(ply.getName(), targetName);
					ply.sendMessage(ChatColor.GREEN + targetName + " has been blocked!");
				}
			} else if (args[0].equalsIgnoreCase("unblock")) {
				if (args.length == 1) {
					ply.sendMessage(ChatColor.RED + "Usage: /phone unblock [player] - To unblock a player. So they can call you.");
				} else {
					Player targetPlayer = Bukkit.getPlayer(args[1]);
					if (targetPlayer == null || !targetPlayer.isOnline()) {
						ply.sendMessage(ChatColor.RED + args[1] + " is either offline or does not exist!");
						return true;
					}
					String targetName = targetPlayer.getName();
					cellH.removeBlock(ply.getName(), targetName);
					ply.sendMessage(ChatColor.GREEN + targetName + " has been unblocked!");
				}
			}
		} else if (cmd.getName().equalsIgnoreCase("global")) {
			AdvancedListener advancedLis = plugin.getAdvancedListener();

			if (advancedLis.disableGlobal.contains(ply.getName())) {
				if (args.length < 1 || !(args[0].equalsIgnoreCase("off"))) {
					advancedLis.disableGlobal.remove(ply.getName());
					ply.sendMessage(ChatColor.GREEN + "You have enabled your global chat!");
				} else {
					ply.sendMessage(ChatColor.GREEN + "You already have global chat disabled!");
				}
			} else {
				if (args.length < 1 || !(args[0].equalsIgnoreCase("on"))) {
					advancedLis.disableGlobal.add(ply.getName());
					ply.sendMessage(ChatColor.GREEN + "You have disabled your global chat!");
				} else {
					ply.sendMessage(ChatColor.GREEN + "You already have global chat enabled!");
				}
			}
		} else if (cmd.getName().equalsIgnoreCase("ac")) {
			if (ply.hasPermission("advancedchat.reload")) {
				if (args[0].equalsIgnoreCase("reload")) {
					plugin.getServer().getPluginManager().disablePlugin(plugin);
					plugin.getServer().getPluginManager().enablePlugin(plugin);
					ply.sendMessage(ChatColor.GREEN + "Reloaded");
				}
			} else {
				ply.sendMessage(ChatColor.RED + "You don't have permission to use this!");
			}
		} else if (cmd.getName().equalsIgnoreCase("l")) {

			/*
			 * if(ply.getName().equalsIgnoreCase("keensta")) {
			 * 
			 * WalkieManager WM = plugin.getWalkieManager();
			 * 
			 * for(int i = 0; i < 9; i++) {
			 * 
			 * int main = i + 1;
			 * 
			 * for(int j = 0; j < 9; j++) {
			 * 
			 * int second = j + 1; String stringR = "";
			 * 
			 * if(WM.doesMainChannelExist(main) == false) { ply.sendMessage(main
			 * + "," + second + " : " + "Empty or Null1"); plugin.log.info(main
			 * + "," + second + " : " + "Empty or Null1"); continue; }
			 * 
			 * if(WM.getRecipients(main, second) == null) { ply.sendMessage(main
			 * + "," + second + " : " + "Empty or Null2"); plugin.log.info(main
			 * + "," + second + " : " + "Empty or Null2"); continue; }
			 * 
			 * if(WM.getRecipients(main, second).isEmpty()) {
			 * ply.sendMessage(main + "," + second + " : " + "Empty or Null3");
			 * plugin.log.info(main + "," + second + " : " + "Empty or Null3");
			 * continue; }
			 * 
			 * for(String player : WM.getRecipients(main, second)) { stringR =
			 * stringR + ", " + player; }
			 * 
			 * ply.sendMessage(stringR); plugin.log.info("Size: " +
			 * WM.getRecipients(main, second).size() + " : " + main + "," +
			 * second + ":" + stringR);
			 * 
			 * } }
			 * 
			 * return true; }
			 */

			if (args.length == 0)
				return true;

			AdvancedListener advancedLis = plugin.getAdvancedListener();

			StringBuilder sb = new StringBuilder();
			for (String args1 : args) {
				sb.append(args1);
				sb.append(" ");
			}

			String message = sb.toString();
			int localRange = plugin.getLocalData().getLocalRange();
			double clearRange = localRange / 2.0;

			List<Entity> localE = ply.getNearbyEntities(localRange, localRange, localRange);
			List<String> localRecipients = new ArrayList<String>();
			List<String> cutOffRecipients = new ArrayList<String>();

			for (Entity e : localE) {
				if (e instanceof Player) {
					Player p = Bukkit.getPlayer(((Player) e).getName());
					double distance = ply.getLocation().distance(p.getLocation());
					if (p.getName() == ply.getName())
						continue;
					if (distance > clearRange) {
						cutOffRecipients.add(p.getName());
						continue;
					}
					localRecipients.add(p.getName());
				}
			}
			localRecipients.add(ply.getName());

			advancedLis.sendLocalMsg(ply, message, localRecipients, cutOffRecipients);
		} else if (cmd.getName().equalsIgnoreCase("spy")) {
			AdvancedListener advancedLis = plugin.getAdvancedListener();

			if (advancedLis.enabledSpy.contains(ply.getName())) {
				advancedLis.enabledSpy.remove(ply.getName());
				ply.sendMessage(ChatColor.GREEN + "Spying disabled!");
				return true;
			} else {
				advancedLis.enabledSpy.add(ply.getName());
				ply.sendMessage(ChatColor.GREEN + "Spying enabled!");
				return true;
			}
		}

		return false;
	}

}
