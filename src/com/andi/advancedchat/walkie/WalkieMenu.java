package com.andi.advancedchat.walkie;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import com.andi.advancedchat.AdvancedChat;
import com.andi.advancedchat.Menu.MenuHandler;
import com.andi.advancedchat.Menu.MenuInteractEvent;
import com.andi.advancedchat.Menu.MenuInteractEventResult;
import com.andi.advancedchat.Menu.MenuOptions;

public class WalkieMenu implements Listener{
	

	private AdvancedChat plugin;
	private MenuHandler menuH;
	
	private static List<Integer> mainChannels = new ArrayList<Integer>();
	private static List<Integer> secondChannels = new ArrayList<Integer>();
	
	static {
		mainChannels.add(14);
		mainChannels.add(1);
		mainChannels.add(4);
		mainChannels.add(5);
		mainChannels.add(11);
		mainChannels.add(9);
		mainChannels.add(10);
		mainChannels.add(15);
		mainChannels.add(0);
		secondChannels.add(1);
		secondChannels.add(14);
		secondChannels.add(11);
		secondChannels.add(10);
		secondChannels.add(4);
		secondChannels.add(6);
		secondChannels.add(5);
		secondChannels.add(0);
		secondChannels.add(15);
	}
	
	public WalkieMenu(AdvancedChat plugin, MenuHandler menuH) {
		this.plugin = plugin;
		this.menuH = menuH;
	}
	
	public void walkieChannelSelection(Player player) {
		LinkedList<MenuOptions> options = new LinkedList<MenuOptions>();
		for(int i = 0; i < 9; i++) {
			options.add(menuH.addOption("Channel "+(i+1), true, Material.WOOL, mainChannels.get(i).shortValue(), i, 0));
		}
		for(int i = 0; i < 9; i++) {
			options.add(menuH.addOption("Sub-Channel "+(i+1), true, Material.INK_SACK, secondChannels.get(i).shortValue(), i, 1));
		}
		menuH.createMenu(player, "ChannelSelection", 18, options, true);
	}
	
	  @EventHandler
	  public void menuInteractEvent(MenuInteractEvent ev) {
		  final Player ply = ev.getPlayer();
		  WalkieManager wm = plugin.getWalkieManager();
		  
		  if(ev.getResult() == MenuInteractEventResult.SELECTED) {
			  String itemName = ev.getSelectedItemName();
			  
			  if(!itemName.contains("Sub-Channel")) {
				  String channel = String.valueOf(itemName.charAt(itemName.length() - 1));
				  
				  wm.setMainChannel(ply.getName(), Integer.parseInt(channel));
				  ply.sendMessage(ChatColor.GREEN + "MainChannel set to " + channel);
				  
				  if(wm.subchannelCheck(ply.getDisplayName())) {

					  if(plugin.getMenuHandler().currentMenus.containsKey(ev.getInventoryView()))
						  plugin.getMenuHandler().currentMenus.remove(ev.getInventoryView());
					  
					  ply.closeInventory();
					  ply.sendMessage(ChatColor.GREEN + "Current Channel " + obtainChannel(ply, wm));
				  }
				  
			  } else {
				  
				  if(wm.channelCheck(ply.getName())) {
					  String channel = String.valueOf(itemName.charAt(itemName.length() - 1));
					 // ply.sendMessage(ChatColor.GREEN + "Sub-Channel set to " + channel);
					  wm.setSecondaryChannel(ply.getName(), Integer.parseInt(channel));
					  ply.sendMessage(ChatColor.GREEN + "Current Channel " + obtainChannel(ply, wm));
					  wm.setSecondaryChannel(ply.getName(), Integer.parseInt(channel));
					  ply.closeInventory();
					
					  return;
				  } else {
					  ply.sendMessage(ChatColor.RED + "Select a main channel first");
				  }
			  }
			 
			  return;
	
		  } 
		  
	  }

	private String obtainChannel(Player ply, WalkieManager wm) {
		int mc = wm.getMainChannel(ply.getName());
		int sc = 0;
		
		if(wm.subchannelCheck(ply.getName())) 
			sc = wm.getSecondChannel(ply.getName());
		
		return mc+"-"+sc;
	}
	
}
