package com.andi.advancedchat.walkie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.andi.advancedchat.AdvancedChat;

public class WalkieManager {
	
	@SuppressWarnings("unused")
	private AdvancedChat plugin;
	
	private HashMap<String, Integer> mainChannel = new HashMap<String, Integer>();
	private HashMap<String, Integer> secondChannel = new HashMap<String, Integer>();
	
	private static HashMap<Integer, HashMap<Integer, List<String>>> channels = new HashMap<Integer, HashMap<Integer, List<String>>>();

	/*static {
		List<String> players = new ArrayList<String>();
		HashMap<Integer, List<String>> secondaryChannels = new HashMap<Integer, List<String>>();
		for(int i = 0; i < 9; i++) {
			secondaryChannels.put(i+1, players);
		}
		for(int i = 0; i < 9; i++) {
			channels.put(i+1, secondaryChannels);
		}
	}*/
	
	public WalkieManager(AdvancedChat plugin) {
		this.plugin = plugin;
	}
	
	public void setMainChannel(String playerName, int channel) {
		int oldMain;
		int oldSecondary;
		
		if(channelCheck(playerName) && subchannelCheck(playerName)) {
			oldMain = mainChannel.get(playerName);
			oldSecondary = secondChannel.get(playerName);
			
			if(doesMainChannelExist(oldMain) && doesSubChannelExist(oldMain, oldSecondary))
				channels.get(oldMain).get(oldSecondary).remove(playerName);
		}
		
		int newMain = channel;
		
		mainChannel.put(playerName, newMain);
		
		if(subchannelCheck(playerName)) {
			oldSecondary = secondChannel.get(playerName);
			
			setSecondaryChannel(playerName, oldSecondary);
		}
		
	}
	
	public void setSecondaryChannel(String playerName, int channel) {
		int main = mainChannel.get(playerName);
		int secondary = channel;
		

		if(channelCheck(playerName) && subchannelCheck(playerName)) {
			int oldMain = mainChannel.get(playerName);
			int oldSecondary = secondChannel.get(playerName);
			
			if(doesMainChannelExist(oldMain) && doesSubChannelExist(oldMain, oldSecondary))
				channels.get(oldMain).get(oldSecondary).remove(playerName);
		}
		
		secondChannel.put(playerName, secondary);
		
		List<String> pL = new ArrayList<String>();
		HashMap<Integer, List<String>> p = new HashMap<Integer, List<String>>();
		
		if(channels.containsKey(main)) {
			if(channels.get(main) == null) {
				p = new HashMap<Integer, List<String>>();
				pL.add(playerName);
			} else {
				p = channels.get(main);
				
				if(p.containsKey(secondary)) {
					if(p.get(secondary) == null) {
						pL = new ArrayList<String>();
						pL.add(playerName);
					} else {
						pL = p.get(secondary);
						
						if(!(pL.contains(playerName)))
							pL.add(playerName);
					}
				}
			}
		} else {
			pL.add(playerName);
		}
		
		p.put(secondary,pL);
		channels.put(main, p);
		
		
	}
	
	public boolean leaveAllChannels(String playerName) {
		int main = 0;
		int secondary = 0;
		if(mainChannel.containsKey(playerName))
			main = mainChannel.get(playerName);
		if(secondChannel.containsKey(playerName))
			secondary = secondChannel.get(playerName);
		
		if(main == 0 || secondary == 0)
			return false;
		
		HashMap<Integer, List<String>> second = channels.get(main);
		
		if(!channels.get(main).containsKey(secondary))
			return true;
		
		List<String> players = channels.get(main).get(secondary);
		
		if(players.contains(playerName))
			players.remove(playerName);
		
		second.put(secondary, players);
		channels.put(main, second);
		mainChannel.remove(playerName);
		secondChannel.remove(playerName);
		return true;
	}
	
	public List<String> getRecipients(int mainChannel, int secondChannel) {
		return channels.get(mainChannel).get(secondChannel);
	}
	
	public int getMainChannel(String playerName) {
		return mainChannel.get(playerName);
	}
	
	public int getSecondChannel(String playerName) {
		return secondChannel.get(playerName);
	}
	
	public boolean channelCheck(String playerName) {
		return mainChannel.containsKey(playerName);
	}
	
	public boolean subchannelCheck(String  playerName) {
		return secondChannel.containsKey(playerName);
	}
	
	public boolean doesMainChannelExist(int i) {
		return channels.containsKey(i);
	}
	
	public boolean doesSubChannelExist(int i, int j) {
		return channels.get(i).containsKey(j);
	}
	
}
