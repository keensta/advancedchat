package com.andi.advancedchat;

import java.util.logging.Logger;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.andi.advancedchat.Menu.MenuHandler;
import com.andi.advancedchat.cell.CellHandler;
import com.andi.advancedchat.cell.CellRinger;
import com.andi.advancedchat.commands.AdvancedCommandHandler;
import com.andi.advancedchat.data.CellChatData;
import com.andi.advancedchat.data.GlobalChatData;
import com.andi.advancedchat.data.LocalChatData;
import com.andi.advancedchat.data.WalkieChatData;
import com.andi.advancedchat.data.WhisperChatData;
import com.andi.advancedchat.listeners.AdvancedListener;
import com.andi.advancedchat.utils.Config;
import com.andi.advancedchat.utils.MsgHandler;
import com.andi.advancedchat.walkie.WalkieManager;
import com.andi.advancedchat.walkie.WalkieMenu;

public class AdvancedChat extends JavaPlugin{
	
	public Logger log = Logger.getLogger("Minecraft");
	
	
	//Classes
	
	//Util
	public Config config = null;
	public MsgHandler msgH = null;
	
	//Data Classes
	public LocalChatData localData = null;
	public GlobalChatData globalData = null;
	public CellChatData cellData = null;
	public WalkieChatData walkieData = null;
	public WhisperChatData whisperData = null;
	
	//Menu
	public MenuHandler menuHandler = null;

	//Cell & Walkie
	public CellRinger cellRinger = null;
	public CellHandler cellHandler = null;
	public WalkieMenu walkieMenu = null;
	public WalkieManager walkieMannager = null;
	
	//Listeners
	public AdvancedListener advancedLis = null;
	
	//Commands
	public AdvancedCommandHandler commandH = null;
	
	//Vars
	public FileConfiguration cfg;
	public boolean vaultEnabled = false;
	public Permission permission;

	@Override
	public void onDisable(){
		
	}
	
	@Override
	public void onEnable() {
		config = new Config(this);
		
		if(getServer().getPluginManager().getPlugin("Vault") != null) {
			vaultEnabled = true;
			if (!setupPermissions()) {
				log.severe("[AdvancedChat] Unable to find permissions plugin!");
			}
		}
		
		config.checkConfig("settings");
		config.checkConfig("format");
		config.checkConfig("ringtone");
		config.checkVersion("settings");
		
		msgH = new MsgHandler(this);
		
		localData = new LocalChatData(this);
		globalData = new GlobalChatData(this);
		cellData = new CellChatData(this);
		walkieData = new WalkieChatData(this);
		whisperData = new WhisperChatData(this);
		
		menuHandler = new MenuHandler(this);
		
		cellHandler = new CellHandler(this);
		walkieMenu = new WalkieMenu(this, menuHandler);
		walkieMannager = new WalkieManager(this);

		advancedLis = new AdvancedListener(this);
		commandH = new AdvancedCommandHandler(this);
	
		cfg = config.loadConfig("settings");
		
		getServer().getPluginManager().registerEvents(walkieMenu, this);
		getServer().getPluginManager().registerEvents(advancedLis, this);
		log.info("[AdvancedChatDemo] AdvancedChat Enabled");
	}
	
	private boolean setupPermissions() {
		final RegisteredServiceProvider<Permission> permissionProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if (permissionProvider != null) {
			permission = permissionProvider.getProvider();
		}
		return (permission != null);
	}

	public Config getconfig() {
		return config;
	}
	
	public LocalChatData getLocalData() {
		return localData;
	}
	
	public GlobalChatData getGlobalData() {
		return globalData;
	}
	
	public CellChatData getCellData() {
		return cellData;
	}
	
	public WalkieChatData getWalkieData() {
		return walkieData;
	}
	
	public WhisperChatData getWhisperData() {
		return whisperData;
	}
	
	public MsgHandler getMsgHandler() {
		return msgH;
	}
	
	public CellHandler getCellHandler() {
		return cellHandler;
	}
	
	public WalkieMenu getWalkieMenu() {
		return walkieMenu;
	}
	
	public WalkieManager getWalkieManager() {
		return walkieMannager;
	}
	
	public AdvancedListener getAdvancedListener() {
		return advancedLis;
	}
	
	public MenuHandler getMenuHandler() {
		return menuHandler;
	}
	
	public FileConfiguration getCfg() {
		return cfg;
	}
	
	public Config getConfigManger() {
		return config;
	}
	
	public boolean getVault() {
		return vaultEnabled;
	}
	
	public Permission getPermission() {
		return permission;
	}
	
	public boolean isInHotBar(Player ply, Material mat) {
		Inventory inv = ply.getInventory();
		for(int i = 0; i < 8; i++) {
			ItemStack item = inv.getItem(i);
			if(item != null) {
				if(item.getType() == mat) {
					return true;
				} else {
					continue;
				}
			}
		}
		return false;
	}
	
}
