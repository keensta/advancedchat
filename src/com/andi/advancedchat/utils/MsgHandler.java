package com.andi.advancedchat.utils;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import com.andi.advancedchat.AdvancedChat;
import com.andi.advancedchat.data.CellChatData;
import com.andi.advancedchat.data.GlobalChatData;
import com.andi.advancedchat.data.LocalChatData;
import com.andi.advancedchat.data.WalkieChatData;
import com.andi.advancedchat.data.WhisperChatData;

public class MsgHandler {

	private AdvancedChat plugin;
	private FileConfiguration cfg;
	
	private double garblePartialChance = 0.40;
	private String messageDrop = "~~~";
	
	public MsgHandler(AdvancedChat plugin) {
		this.plugin = plugin;
		this.cfg = plugin.getconfig().loadConfig("settings");
		

		this.garblePartialChance = cfg.getDouble("garblePartialChance");
		this.messageDrop = cfg.getString("messageDrop");
	}
	
	/**
	 *  	Removes letters from the message the further the targetplayer
	 * @param message Message that needs garbling
	 * @param calrity The calrity of the message
	 * @param type Type of chat, Used to get the message colour.
	 * @return Modifyed message
	 */
	public String garbleMessage(String message, double clarity, String type) {
		Random ran = new Random();
		/*LocalChatData localData = plugin.getLocalData();*/

		StringBuilder str = new StringBuilder();
		
/*		if(distance > localData.getLocalRange())
			garbleChance = garbleChance + (distance / 100);
		
		if(distance < (localData.getLocalCutOff() - (localData.getLocalRange() / 2)))
			garbleChance = garbleChance - (distance / 100);*/
		
		int i = 0;
		int drops = 0;
		while(i < message.length()) {
			int c = message.codePointAt(i);
			i += Character.charCount(c);

				if(ran.nextDouble() < clarity) {
						str.appendCodePoint(c);
						continue;
				} else {
					if((ran.nextDouble()) < garblePartialChance) {
						str.append(ChatColor.GRAY.toString());
						str.appendCodePoint(c);
						str.append(" ");
						if(getMessageColour(type).contains("[")) {
							str.append(setColour(getMessageColour(type)));
							i += 2;
							continue;
						}
						str.append(setColour(getMessageColour(type)));
						i += 2;
						continue;
					} 
					if(message.charAt(i-1) == Character.SPACE_SEPARATOR) {
						str.append(" ");
						drops += 1;
						continue;
					}
					str.append(" ");
					drops += 1;
			}
				
			if(drops == message.length()) {
				//Returns messageDrop due to all the message being lost
				return messageDrop;
			}
		}

		return str.toString();
	}
	
	public String getMsgFormat(String playerDisplayName, String playerName, String message, String type, String oldFormat) {
		switch (type) {
		case "Global":
			
				GlobalChatData globalData = plugin.getGlobalData();
				return setColour(globalData.getMsgFormat(playerDisplayName, playerName, message, oldFormat));
				
		case "Local":
			
				LocalChatData localData = plugin.getLocalData();
				return setColour(localData.getMsgFormat(playerDisplayName, playerName, message, oldFormat));		
				
		case "Cell":
				
				CellChatData cellData = plugin.getCellData();
				return setColour(cellData.getMsgFormat(playerDisplayName, playerName, message, oldFormat));
			
		case "Walkie":
			
				WalkieChatData walkieData = plugin.getWalkieData();
				return setColour(walkieData.getMsgFormat(playerDisplayName, playerName, message, oldFormat));
		
		case "Whisper":
				WhisperChatData whisperData = plugin.getWhisperData();
				return setColour(whisperData.getMsgFormat(playerDisplayName, playerName, message, oldFormat));
		}
		return message;
	}
	
	public String getMessageColour(String type) {
		switch (type) {
		case "Global":
			
				GlobalChatData globalData = plugin.getGlobalData();
				return globalData.getMessageColour();
				
		case "Local":
			
				LocalChatData localData = plugin.getLocalData();
				return localData.getMessageColour();		
				
		case "Cell":
				
				CellChatData cellData = plugin.getCellData();
				return cellData.getMessageColour();
			
		case "Walkie":
			
				WalkieChatData walkieData = plugin.getWalkieData();
				return walkieData.getMessageColour();
		
		case "Whisper":
				WhisperChatData whisperData = plugin.getWhisperData();
				return whisperData.getMessageColour();
		}
		return "[White]";
	}
	
	public String setColour(String cMessage) {
		cMessage = setColourSymbol(cMessage);
		cMessage = cMessage.replace("[Black]", ChatColor.BLACK.toString());
		cMessage = cMessage.replace("[Dark_Blue]", ChatColor.DARK_BLUE.toString());
		cMessage = cMessage.replace("[Dark_Green]", ChatColor.DARK_GREEN.toString());
		cMessage = cMessage.replace("[Dark_Aqua]", ChatColor.DARK_AQUA.toString());
		cMessage = cMessage.replace("[Dark_Red]", ChatColor.DARK_RED.toString());
		cMessage = cMessage.replace("[Dark_Purple]", ChatColor.DARK_PURPLE.toString());
		cMessage = cMessage.replace("[Dark_Grey]", ChatColor.DARK_GRAY.toString());
		cMessage = cMessage.replace("[Gold]", ChatColor.GOLD.toString());
		cMessage = cMessage.replace("[Gray]", ChatColor.GRAY.toString());
		cMessage = cMessage.replace("[Blue]", ChatColor.BLUE.toString());
		cMessage = cMessage.replace("[Green]", ChatColor.GREEN.toString());
		cMessage = cMessage.replace("[Aqua]", ChatColor.AQUA.toString());
		cMessage = cMessage.replace("[Red]", ChatColor.RED.toString());
		cMessage = cMessage.replace("[Yellow]", ChatColor.YELLOW.toString());
		cMessage = cMessage.replace("[White]", ChatColor.WHITE.toString());
		cMessage = cMessage.replace("[Light_Purple]", ChatColor.LIGHT_PURPLE.toString());
		return cMessage;
	}
	
	public String setColourSymbol(String cMessage) {
		return cMessage.replaceAll("(?i)&([0-9A-FKLMNOR])", "\u00A7$1");
	}
	
}
