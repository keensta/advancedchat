package com.andi.advancedchat.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.andi.advancedchat.AdvancedChat;

public class Config {

	public AdvancedChat plugin;
	
	public Config(AdvancedChat plugin) {
		this.plugin = plugin;	
	}
	
	
	public void checkConfig(String name) {
		final File dataDirectory = plugin.getDataFolder();
		if (dataDirectory.exists() == false)
			dataDirectory.mkdirs();

		final File f = new File(plugin.getDataFolder(), name+".yml");
		if (f.canRead())
			return;

		plugin.log.info("[AdvancedChat] Writing default " + name+".yml");
		final InputStream in = plugin.getResource(name+".yml");

		if (in == null) {
			plugin.log.severe("[AdvancedChat] Could not find " + name+".yml" + " resource");
			return;
		} else {
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(f);
				final byte[] buf = new byte[512];
				int len;
				while ((len = in.read(buf)) > 0) {
					fos.write(buf, 0, len);
				}
			} catch (final IOException iox) {
				plugin.log.severe("[AdvancedChat] Could not export " + name+".yml");
				return;
			} finally {
				if (fos != null)
					try {
						fos.close();
					} catch (final IOException iox) {
					}
				if (in != null)
					try {
						in.close();
					} catch (final IOException iox) {
					}
			}
			return;
		}
	}
	
	public FileConfiguration loadConfig(String name) {
		return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), name+".yml"));
	}

	public void saveConfig(FileConfiguration cfg, String name) {
		try {
			cfg.save(new File(plugin.getDataFolder(), name+".yml"));
		} catch (IOException e) {
			Logger.getLogger("Minecraft").severe("[AdvancedChat] Failed to save " + name +".yml!");
			e.printStackTrace();
		}
	}
	
	public void checkVersion(String name) {
		FileConfiguration cfg = loadConfig(name);
		
		double version = cfg.getDouble("Version");
		
		if(name.equalsIgnoreCase("settings")) {
			if(version == 0.2) {
				plugin.log.info("Settings.yml upto date");
				return;
			}	
			plugin.log.info("Settings.yml updating");
			cfg.set("Version", 0.2);
			cfg.set("unRecived." + "walkieNormal", "Your walkie channel is empty!");
			saveConfig(cfg, "settings");
		} else if(name.equalsIgnoreCase("format")) {
			if(version == 0.2)
				return;
		}
		
		
	}
	
	
}
