package com.andi.advancedchat.cell;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.andi.advancedchat.AdvancedChat;

public class CellManager {

	@SuppressWarnings("unused")
	private AdvancedChat plugin;

	private List<String> players = new LinkedList<String>();

	public CellManager(AdvancedChat plugin, String playerName) {
		this.plugin = plugin;

		players.add(playerName);
	}

	public List<String> getCallParticipants() {
		return players;
	}

	public void addCallParticipant(String playerName) {
		Player ply = Bukkit.getPlayer(playerName);

		ply.sendMessage(ChatColor.GREEN + "You have been connected to the call!");
		players.add(playerName);

		if(!players.isEmpty()) {

			for(String p : players) {
				if(p.equals(playerName))
					continue;

				Player player = Bukkit.getPlayer(p);
				player.sendMessage(ChatColor.GREEN + playerName + " has been added to the call");
			}
		}

	}

	public void kickCallParticipant(String playerName) {
		Player ply = Bukkit.getPlayer(playerName);

		players.remove(playerName);
		ply.sendMessage("You have been kicked from the call!");

		if(players.size() == 1)
			hangUpCall();
	}

	public void hangUpCall() {
		for(int i = 0; i < players.size(); i++) {
			Player ply = Bukkit.getPlayer(players.get(i));

			if(ply.isOnline())
				ply.sendMessage(ChatColor.GREEN + "Call has ended!");
		}

		players.clear();
	}

}
