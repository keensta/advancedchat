package com.andi.advancedchat.cell;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.andi.advancedchat.AdvancedChat;

public class CellRinger implements Runnable {

	public AdvancedChat plugin;
	private FileConfiguration cfg;
	private FileConfiguration ringCfg;

	public CellRinger(AdvancedChat plugin) {
		this.plugin = plugin;
		this.cfg = plugin.getconfig().loadConfig("settings");
		this.ringCfg = plugin.getconfig().loadConfig("ringtone");

		this.ringTimes = cfg.getInt("ringTimes");
		this.ringTone = ringCfg.getStringList("ringtone");
		this.delay = getDelay();

	}

	private int getDelay() {
		String note = ringTone.get(ringTone.size() - 1);
		String[] components = note.split(":");
		return Integer.parseInt(components[2]);
	}

	private int taskId;
	private int i;
	private Player player;
	public Player callingPlayer;
	private List<String> ringTone = new ArrayList<String>();
	private int delay; // Delay of last note.. Should stop sound classing.
	private int ringTimes = 5;
	private boolean stopRing;

	public void ringPlayer(String playerName, String playerCalling) {
		player = Bukkit.getPlayer(playerName);
		callingPlayer = Bukkit.getPlayer(playerCalling);

		plugin.getCellHandler().addRingingCall(playerName, playerCalling);
		player.sendMessage(callingPlayer.getName() + " is calling you!");

		taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, this, 30, 20);

		manageRingTone();
	}

	public void run() {
		int newDelay = delay * ringTimes;

		if(i >= ringTimes) {
			stop(newDelay);
		}

	}

	public void stop(int newDelay) {
		Bukkit.getScheduler().cancelTask(taskId);

		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				stopRing = true;

				if(!plugin.getCellHandler().checkForCall(player.getName())) {
					callingPlayer.sendMessage(ChatColor.RED + "Is busy and can't answer the phone!");
					plugin.getAdvancedListener().usingCell.remove(callingPlayer.getName());
				}

				plugin.getCellHandler().removeRingingCall(player.getName());
			}
		}, newDelay);

	}

	public void manageRingTone() {

		if(i >= ringTimes)
			return;
		if(i == 0) {
			startRingTone();
			i += 1;
			manageRingTone();
		}

		if(ringTimes > 1) {
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {

					if(stopRing)
						return;

					startRingTone();
					i += 1;
					manageRingTone();
				}
			}, delay * i + 2);

		}
	}

	private void startRingTone() {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Iterator<String> ringToneNotes = ringTone.iterator();

				while (ringToneNotes.hasNext()) {

					String structure = (String) ringToneNotes.next();
					String[] components = structure.split(":");
					Sound sound = Sound.valueOf(components[0]);
					int noteId = Integer.parseInt(components[1]);
					float pitch = getPitch(noteId);
					int tempo = Integer.parseInt(components[2]);
					Location l = player.getLocation();
					reproductor(tempo, l, sound, pitch);

				}

			}
		}, 0);

	}

	private void reproductor(int tempo, final Location l, final Sound sound, final float pitch) {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				if(stopRing)
					return;

				l.getWorld().playSound(l, sound, 1.0F, pitch);
			}

		}, tempo);
	}

	float getPitch(int id) {
		switch (id) {
		case 0:
			return (float) 0.5;
		case 1:
			return (float) 0.53;
		case 2:
			return (float) 0.56;
		case 3:
			return (float) 0.6;
		case 4:
			return (float) 0.63;
		case 5:
			return (float) 0.67;
		case 6:
			return (float) 0.7;
		case 7:
			return (float) 0.76;
		case 8:
			return (float) 0.8;
		case 9:
			return (float) 0.84;
		case 10:
			return (float) 0.9;
		case 11:
			return (float) 0.94;
		case 12:
			return (float) 1.0;
		case 13:
			return (float) 1.06;
		case 14:
			return (float) 1.12;
		case 15:
			return (float) 1.18;
		case 16:
			return (float) 1.26;
		case 17:
			return (float) 1.34;
		case 18:
			return (float) 1.42;
		case 19:
			return (float) 1.5;
		case 20:
			return (float) 1.6;
		case 21:
			return (float) 1.68;
		case 22:
			return (float) 1.78;
		case 23:
			return (float) 1.88;
		case 24:
			return (float) 2.0;
		}
		return 0.0F;
	}

}
