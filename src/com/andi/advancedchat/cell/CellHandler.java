package com.andi.advancedchat.cell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.andi.advancedchat.AdvancedChat;

public class CellHandler {

	private AdvancedChat plugin;

	private HashMap<String, CellManager> activeCalls = new HashMap<String, CellManager>();
	private HashMap<String, String> ringingCalls = new HashMap<String, String>();
	private HashMap<String, List<String>> blockedCallers = new HashMap<String, List<String>>();

	public CellHandler(AdvancedChat plugin) {
		this.plugin = plugin;

	}

	public void setUpCall(String playerName, String playerName2) {
		CellManager cm = new CellManager(plugin, playerName);

		cm.addCallParticipant(playerName2);

		activeCalls.put(playerName, cm);
		activeCalls.put(playerName2, cm);
	}

	public void hangUp(String playerName) {
		CellManager cm = activeCalls.get(playerName);
		List<String> callParticipants = cm.getCallParticipants();

		for(String member : callParticipants) {
			if(activeCalls.containsKey(member))
				activeCalls.remove(member);
		}

		cm.hangUpCall();

		if(activeCalls.containsKey(playerName))
			activeCalls.remove(playerName);
	}

	public CellManager getCellManager(String playerName) {
		return activeCalls.get(playerName);
	}

	public boolean checkForCall(String playerName) {
		return activeCalls.containsKey(playerName);
	}

	public boolean hasRingingCall(String playerName) {
		return ringingCalls.containsKey(playerName);
	}

	public String getRinger(String playerName) {
		return ringingCalls.get(playerName);
	}

	public void addRingingCall(String playerName, String playerCalling) {
		ringingCalls.put(playerName, playerCalling);
	}

	public void removeRingingCall(String playerName) {
		ringingCalls.remove(playerName);
	}

	public void addBlock(String playerName, String blockName) {
		if(blockedCallers.containsKey(playerName)) {
			List<String> blockedPlayers = blockedCallers.get(playerName);

			if(!blockedPlayers.contains(blockName))
				blockedPlayers.add(blockName);

			blockedCallers.put(playerName, blockedPlayers);
		} else {
			List<String> blockedPlayers = new ArrayList<String>();

			blockedPlayers.add(blockName);
			blockedCallers.put(playerName, blockedPlayers);
		}
	}

	public void removeBlock(String playerName, String unblockName) {
		if(blockedCallers.containsKey(playerName)) {
			List<String> blockedPlayers = blockedCallers.get(playerName);

			if(blockedPlayers.contains(unblockName))
				blockedPlayers.remove(unblockName);

			blockedCallers.put(playerName, blockedPlayers);
		}
	}

	public boolean isBlocked(String playerName, String targetName) {
		if(!blockedCallers.containsKey(playerName))
			return false;

		List<String> blockedPlayers = blockedCallers.get(playerName);

		if(!blockedPlayers.contains(targetName))
			return false;

		return true;
	}

	public List<String> listBlockedCallers(String playerName) {
		if(blockedCallers.containsKey(playerName)) {
			return blockedCallers.get(playerName);
		} else {
			return null;
		}
	}

}
