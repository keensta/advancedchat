package com.andi.advancedchat.data;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.andi.advancedchat.AdvancedChat;

public class LocalChatData implements ChatData{
	
	public AdvancedChat plugin;
	public FileConfiguration cfg;
	public FileConfiguration cfgFormat;
	
	public int localRange = 60;
	public String msgFormat = "[PrefixColour][prefix] [PlayerColour]<[PlayerName]>[messageColour] [Message]";
	public String prefix = "[Local]";
	public String playerColour = "[White]";
	public boolean setSenderColour = false;
	public String senderColour = "[Green]";
	public String messageColour = "[White]";
	public String prefixColour = "[Yellow]";
	public String unRecived = "Your message falls to deaf ears!";
	
	public LocalChatData(AdvancedChat plugin) {
		this.plugin = plugin;
		this.cfg = plugin.getconfig().loadConfig("settings");
		this.cfgFormat  = plugin.getconfig().loadConfig("format");
		
		this.localRange = cfg.getInt("localRange");
		this.unRecived = cfg.getString("unRecived." + "normal");
		
		if(cfgFormat.contains("localFormat")) {
			this.msgFormat = cfgFormat.getString("localFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("localFormat." + "prefix");
			this.playerColour = cfgFormat.getString("localFormat." + "playerColour");
			this.setSenderColour = cfgFormat.getBoolean("localFormat." + "setSenderColour");
			this.senderColour = cfgFormat.getString("localFormat." + "senderColour");
			this.messageColour = cfgFormat.getString("localFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("localFormat." + "prefixColour");
		}
	}
	
	public boolean updateFormat() {
		this.localRange = cfg.getInt("localRange");
		this.unRecived = cfg.getString("unRecived." + "normal");
		if(cfgFormat.contains("localFormat")) {
			this.msgFormat = cfgFormat.getString("localFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("localFormat." + "prefix");
			this.playerColour = cfgFormat.getString("localFormat." + "playerColour");
			this.setSenderColour = cfgFormat.getBoolean("localFormat." + "setSenderColour");
			this.senderColour = cfgFormat.getString("localFormat." + "senderColour");
			this.messageColour = cfgFormat.getString("localFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("localFormat." + "prefixColour");
			return true;
		}
		return false;
	}
	
	public int getLocalRange() {
		return localRange;
	}
	
	public String getunRecived(Player player) {
		if(plugin.vaultEnabled) {
			String group = plugin.getPermission().getPrimaryGroup(player);
			if(cfg.contains("unRecived." + group)) {
				return plugin.getMsgHandler().setColour(cfg.getString("unRecived." + group));
			} else {
				return plugin.getMsgHandler().setColour(unRecived);
			}
		}
		
		return plugin.getMsgHandler().setColour(unRecived);
	}
	
	public boolean setSenderColour() {
		return setSenderColour;
	}
	
	public String getSenderColour() {
		return senderColour;
	}
	
	public String getMessageColour() {
		return messageColour;
	}
	
	public String getMsgFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String newMessage = setFormat(playerDisplayName, playerName, Message, oldFormat);
		return newMessage;
	}
	
	public String setFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String format = msgFormat;
		format = format.replace("[PrefixColour]", prefixColour);
		format = format.replace("[prefix]", prefix);
		format = format.replace("[PlayerColour]", playerColour);
		format = format.replace("[PlayerName]", playerName);
		format = format.replace("[PlayerDisplayName]", playerDisplayName);
		format = format.replace("[messageColour]", messageColour);
		format = format.replace("[Message]", Message.trim());
		return format;
	}


/*	private CharSequence getEssFormat(String playerName) {
		IEssentials ess = plugin.getEssentials();
		Player ply = Bukkit.getPlayer(playerName);
		User user = ess.getUser(ply);
		String group = user.getGroup();
		MessageFormat format1 = ess.getSettings().getChatFormat(group);
		plugin.getlogger().info("Ess format: " + format1);
		String format = ess.getSettings().getChatFormat(group).toString();
		plugin.getlogger().info("Ess format String : " + format);
		return format;
	}*/
	
}
