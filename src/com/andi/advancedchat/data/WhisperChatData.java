package com.andi.advancedchat.data;

import org.bukkit.configuration.file.FileConfiguration;

import com.andi.advancedchat.AdvancedChat;

public class WhisperChatData implements ChatData{
	
	public AdvancedChat plugin;
	public FileConfiguration cfg;
	public FileConfiguration cfgFormat;
	
	public int whisperRange = 10;
	public String msgFormat = "[PrefixColour][prefix] [PlayerColour]<[PlayerName]>[messageColour] [Message]";
	public String prefix = "[Whisper]";
	public String playerColour = "[White]";
	public boolean setSenderColour = false;
	public String senderColour = "[Green]";
	public String messageColour = "[Yellow]";
	public String prefixColour = "[Blue]";
	
	public WhisperChatData(AdvancedChat plugin) {
		this.plugin = plugin;
		this.cfg = plugin.getconfig().loadConfig("settings");
		this.cfgFormat  = plugin.getconfig().loadConfig("format");
		
		this.whisperRange = cfg.getInt("whisperRange");
		
		if(cfgFormat.contains("whisperFormat")) {
			this.msgFormat = cfgFormat.getString("whisperFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("whisperFormat." + "prefix");
			this.playerColour = cfgFormat.getString("whisperFormat." + "playerColour");
			this.setSenderColour = cfgFormat.getBoolean("whisperFormat." + "setSenderColour");
			this.senderColour = cfgFormat.getString("whisperFormat." + "senderColour");
			this.messageColour = cfgFormat.getString("whisperFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("whisperFormat." + "prefixColour");
		}
	}

	public boolean updateFormat() {
		this.whisperRange = cfg.getInt("whisperRange");
		if(cfgFormat.contains("whisperFormat")) {
			this.msgFormat = cfgFormat.getString("whisperFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("whisperFormat." + "prefix");
			this.playerColour = cfgFormat.getString("whisperFormat." + "playerColour");
			this.setSenderColour = cfgFormat.getBoolean("whisperFormat." + "setSenderColour");
			this.senderColour = cfgFormat.getString("whisperFormat." + "senderColour");
			this.messageColour = cfgFormat.getString("whisperFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("whisperFormat." + "prefixColour");
			return true;
		}
		return false;
	}
	
	public int getWhisperRange() {
		return whisperRange;
	}
	
	public boolean setSenderColour() {
		return setSenderColour;
	}
	
	public String getSenderColour() {
		return senderColour;
	}
	
	public String getMessageColour() {
		return messageColour;
	}

	@Override
	public String getMsgFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String newMessage = setFormat(playerDisplayName, playerName, Message, oldFormat);
		return newMessage;
	}

	@Override
	public String setFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String format = msgFormat;
		format = format.replace("[PrefixColour]", prefixColour);
		format = format.replace("[prefix]", prefix);
		format = format.replace("[PlayerColour]", playerColour);
		format = format.replace("[PlayerName]", playerName);
		format = format.replace("[PlayerDisplayName]", playerDisplayName);
		format = format.replace("[messageColour]", messageColour);
		format = format.replace("[Message]", Message.trim());
		return format;
	}

}
