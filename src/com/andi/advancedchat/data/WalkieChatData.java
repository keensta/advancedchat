package com.andi.advancedchat.data;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.andi.advancedchat.AdvancedChat;
import com.andi.advancedchat.walkie.WalkieManager;

public class WalkieChatData implements ChatData{

	public AdvancedChat plugin;
	public FileConfiguration cfg;
	public FileConfiguration cfgFormat;
	private WalkieManager walkieM;
	
	public String msgFormat = "[PrefixColour][prefix] [PlayerColour]<[PlayerName]> [messageColour][Message]";
	public String prefix = "[Channel]";
	public String playerColour = "[White]";
	public boolean setSenderColour = false;
	public String senderColour = "[Green]";
	public String messageColour = "[Blue]";
	public String prefixColour = "[Blue]";
	public String unRecived = "Your walkie channel is empty!";
	
	public WalkieChatData(AdvancedChat plugin) {
		this.plugin = plugin;
		this.cfg = plugin.getconfig().loadConfig("settings");
		this.cfgFormat = plugin.getconfig().loadConfig("format");
		
		this.unRecived = cfg.getString("unRecived." + "walkieNormal");
		
		if(cfgFormat.contains("walkieFormat")) {
			this.msgFormat = cfgFormat.getString("walkieFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("walkieFormat." + "prefix");
			this.playerColour = cfgFormat.getString("walkieFormat." + "playerColour");
			this.setSenderColour = cfgFormat.getBoolean("walkieFormat." + "setSenderColour");
			this.senderColour = cfgFormat.getString("walkieFormat." + "senderColour");
			this.messageColour = cfgFormat.getString("walkieFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("walkieFormat." + "prefixColour");
		}
	}
	
	public boolean updateFormat() {
		this.unRecived = cfg.getString("unRecived." + "walkieNormal");
		if(cfgFormat.contains("walkieFormat")) {
			this.msgFormat = cfgFormat.getString("walkieFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("walkieFormat." + "prefix");
			this.playerColour = cfgFormat.getString("walkieFormat." + "playerColour");
			this.setSenderColour = cfgFormat.getBoolean("walkieFormat." + "setSenderColour");
			this.senderColour = cfgFormat.getString("walkieFormat." + "senderColour");
			this.messageColour = cfgFormat.getString("walkieFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("walkieFormat." + "prefixColour");
			return true;
		}
		return false;
	}
	
	public String getunRecived(Player player) {
		if(plugin.vaultEnabled) {
			String group = plugin.getPermission().getPrimaryGroup(player);
			if(cfg.contains("unRecived." + group)) {
				return plugin.getMsgHandler().setColour(cfg.getString("unRecived." + group));
			} else {
				return plugin.getMsgHandler().setColour(unRecived);
			}
		}
		
		return plugin.getMsgHandler().setColour(unRecived);
	}
	
	public boolean setSenderColour() {
		return setSenderColour;
	}
	
	public String getSenderColour() {
		return senderColour;
	}
	
	public String getMessageColour() {
		return messageColour;
	}
	
	public String getMsgFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String newMessage = setFormat(playerDisplayName, playerName, Message, oldFormat);
		return newMessage;
	}
	
	public String setFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		this.walkieM = plugin.getWalkieManager();
		String format = msgFormat;
		format = format.replace("[PrefixColour]", prefixColour);
		format = format.replace("[prefix]", prefix);
		format = format.replace("[Channel]", walkieM.getMainChannel(playerName)+"-"+walkieM.getSecondChannel(playerName));
		format = format.replace("[PlayerColour]", playerColour);
		format = format.replace("[PlayerName]", playerName);
		format = format.replace("[PlayerDisplayName]", playerDisplayName);
		format = format.replace("[messageColour]", messageColour);
		format = format.replace("[Message]", Message.trim());
		return format;
	}
}
