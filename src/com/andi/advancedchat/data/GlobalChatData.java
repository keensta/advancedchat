package com.andi.advancedchat.data;

import org.bukkit.configuration.file.FileConfiguration;

import com.andi.advancedchat.AdvancedChat;

public class GlobalChatData implements ChatData{
	
	public AdvancedChat plugin;
	public FileConfiguration cfgFormat;
	
	public String msgFormat = "[PrefixColour][prefix] [PlayerColour]<[PlayerName]>[messageColour] [Message]";
	public String prefix = "[Global]";
	public String playerColour = "[White]";
	public String messageColour = "[Yellow]";
	public String prefixColour = "[Yellow]";
	
	public GlobalChatData(AdvancedChat plugin) {
		this.plugin = plugin;
		this.cfgFormat = plugin.getconfig().loadConfig("format");
		
		if(cfgFormat.contains("globalFormat")) {
			this.msgFormat = cfgFormat.getString("globalFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("globalFormat." + "prefix");
			this.playerColour = cfgFormat.getString("globalFormat." + "playerColour");
			this.messageColour = cfgFormat.getString("globalFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("globalFormat." + "prefixColour");
		}
	}
	
	public boolean updateFormat() {
		if(cfgFormat.contains("globalFormat")) {
			this.msgFormat = cfgFormat.getString("globalFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("globalFormat." + "prefix");
			this.playerColour = cfgFormat.getString("globalFormat." + "playerColour");
			this.messageColour = cfgFormat.getString("globalFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("globalFormat." + "prefixColour");
			return true;
		}
		return false;
	}
	
	public String getMessageColour() {
		return messageColour;
	}
	
	@Override
	public boolean setSenderColour() {
		return false;
	}

	@Override
	public String getSenderColour() {
		return null;
	}
	
	public String getMsgFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String newMessage = setFormat(playerDisplayName, playerName, Message, oldFormat);
		return newMessage;
	}
	
	public String setFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String format = msgFormat;
		format = format.replace("[PrefixColour]", prefixColour);
		format = format.replace("[prefix]", prefix);
		format = format.replace("[PlayerColour]", playerColour);
		format = format.replace("[Essentials-format]", oldFormat);
		format = format.replace("[PlayerName]", playerName);
		format = format.replace("[PlayerDisplayName]", playerDisplayName);
		format = format.replace("[messageColour]", messageColour);
		format = format.replace("[Message]", Message.trim());
		return format;
	}
}
