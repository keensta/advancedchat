package com.andi.advancedchat.data;

public interface ChatData {
	
	public boolean setSenderColour();
	
	public String getSenderColour();
	
	public String getMessageColour();
	
	public String getMsgFormat(String playerDisplayName, String playerName, String Message, String oldFormat);
	
	public String setFormat(String playerDisplayName, String playerName, String Message, String oldFormat);
	
}
