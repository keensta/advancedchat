package com.andi.advancedchat.data;

import org.bukkit.configuration.file.FileConfiguration;

import com.andi.advancedchat.AdvancedChat;

public class CellChatData implements ChatData{

	public AdvancedChat plugin;
	public FileConfiguration cfgFormat;
	
	public String msgFormat = "[PrefixColour][prefix] [PlayerColour]<[PlayerName]>[messageColour] [Message]";
	public String prefix = "[Cell]";
	public String playerColour = "[White]";
	public boolean setSenderColour = false;
	public String senderColour = "[Green]";
	public String messageColour = "[Green]";
	public String prefixColour = "[Green]";
	
	public CellChatData(AdvancedChat plugin) {
		this.plugin = plugin;
		this.cfgFormat = plugin.getconfig().loadConfig("format");
		
		if(cfgFormat.contains("cellFormat")) {
			this.msgFormat = cfgFormat.getString("cellFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("cellFormat." + "prefix");
			this.playerColour = cfgFormat.getString("cellFormat." + "playerColour");
			this.setSenderColour = cfgFormat.getBoolean("cellFormat." + "setSenderColour");
			this.senderColour = cfgFormat.getString("cellFormat." + "senderColour");
			this.messageColour = cfgFormat.getString("cellFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("cellFormat." + "prefixColour");
		}
	}
	
	public boolean updateFormat() {
		if(cfgFormat.contains("cellFormat")) {
			this.msgFormat = cfgFormat.getString("cellFormat." + "msgFormat");
			this.prefix = cfgFormat.getString("cellFormat." + "prefix");
			this.playerColour = cfgFormat.getString("cellFormat." + "playerColour");
			this.setSenderColour = cfgFormat.getBoolean("cellFormat." + "setSenderColour");
			this.senderColour = cfgFormat.getString("cellFormat." + "senderColour");
			this.messageColour = cfgFormat.getString("cellFormat." + "messageColour");
			this.prefixColour = cfgFormat.getString("cellFormat." + "prefixColour");
			return true;
		}
		return false;
	}
	
	public boolean setSenderColour() {
		return setSenderColour;
	}
	
	public String getSenderColour() {
		return senderColour;
	}
	
	public String getMessageColour() {
		return messageColour;
	}
	
	public String getMsgFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String newMessage = setFormat(playerDisplayName, playerName, Message, oldFormat);
		return newMessage;
	}
	
	public String setFormat(String playerDisplayName, String playerName, String Message, String oldFormat) {
		String format = msgFormat;
		format = format.replace("[PrefixColour]", prefixColour);
		format = format.replace("[prefix]", prefix);
		format = format.replace("[PlayerColour]", playerColour);
		format = format.replace("[PlayerName]", playerName);
		format = format.replace("[PlayerDisplayName]", playerDisplayName);
		format = format.replace("[messageColour]", messageColour);
		format = format.replace("[Message]", Message.trim());
		return format;
	}
	
}
