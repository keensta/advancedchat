package com.andi.advancedchat.Menu;

//Credit to H2NCH2COOH for MenuEvent Idea
public enum MenuInteractEventResult {
	SELECTED, CLOSED, QUITCLOSED;
}