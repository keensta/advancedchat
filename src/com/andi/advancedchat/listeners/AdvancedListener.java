package com.andi.advancedchat.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.andi.advancedchat.AdvancedChat;
import com.andi.advancedchat.cell.CellHandler;
import com.andi.advancedchat.cell.CellRinger;
import com.andi.advancedchat.data.CellChatData;
import com.andi.advancedchat.data.LocalChatData;
import com.andi.advancedchat.data.WalkieChatData;
import com.andi.advancedchat.data.WhisperChatData;
import com.andi.advancedchat.utils.MsgHandler;
import com.andi.advancedchat.walkie.WalkieManager;

public class AdvancedListener implements Listener {

	public AdvancedChat plugin;
	private WalkieManager walkieM;
	private CellHandler cellH;
	private MsgHandler msgH;

	public List<String> usingCell = new ArrayList<String>();
	public HashMap<String, CellRinger> callingCalls = new HashMap<String, CellRinger>();
	public HashMap<String, Long> lastGlobalMsg = new HashMap<String, Long>();
	public List<String> disableGlobal = new ArrayList<String>();
	public List<String> enabledSpy = new ArrayList<String>();

	public AdvancedListener(AdvancedChat plugin) {
		this.plugin = plugin;

		this.walkieM = plugin.getWalkieManager();
		this.cellH = plugin.getCellHandler();
		this.msgH = plugin.getMsgHandler();
	}

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent ev) {
		Material m = Material.WATCH;
		Material mCompass = Material.COMPASS;
		Item droppedI = ev.getItemDrop();
		if (droppedI.getItemStack().getType() == m) {
			String playerName = ev.getPlayer().getName();
			if (cellH.checkForCall(playerName)) {
				cellH.hangUp(playerName);
			}
		} else if (droppedI.getItemStack().getType() == mCompass) {
			if (walkieM.leaveAllChannels(ev.getPlayer().getName())) {
				ev.getPlayer().sendMessage(ChatColor.GREEN + "Your walkie connection has been lost!");
			}
		}
	}

	/*
	 * @EventHandler public void onIntentoryClick(InventoryClickEvent ev) {
	 * Material m = Material.WATCH; if(!(ev.getInventory().getType() ==
	 * InventoryType.PLAYER)) return; if(ev.getCurrentItem() == null) return;
	 * ItemStack item = ev.getCurrentItem(); if(item.getType() == m) { String
	 * playerName = ev.getInventory().getViewers().get(0).getName();
	 * if(cellH.hasRingingCall(playerName)) { cellH.setUpCall(playerName,
	 * cellH.getRinger(playerName)); CellRinger cellR =
	 * callingCalls.get(playerName); cellR.stop();
	 * callingCalls.remove(playerName); return; }
	 * 
	 * }
	 * 
	 * }
	 */

	@EventHandler
	public void onPlayerItem(PlayerItemHeldEvent ev) {
		int slot = ev.getNewSlot();
		Player ply = ev.getPlayer();
		String playerName = ply.getName();
		if (ply.getInventory().getItem(slot) == null) {
			usingCell.remove(ply.getName());
			return;
		}
		if (ply.getInventory().getItem(slot).getType() == Material.WATCH) {
			if (cellH.checkForCall(playerName))
				return;
			if (cellH.hasRingingCall(playerName)) {
				cellH.setUpCall(playerName, cellH.getRinger(playerName));
				CellRinger cellR = callingCalls.get(playerName);
				cellR.stop(0);
				callingCalls.remove(playerName);
				return;
			}
		} else {
			if (usingCell.contains(playerName)) {
				usingCell.remove(playerName);
			}
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent ev) {
		Player ply = ev.getPlayer();
		String playerName = ply.getName();
		if (ev.getAction() == Action.RIGHT_CLICK_AIR || ev.getAction() == Action.RIGHT_CLICK_BLOCK) {

			if (ply.getItemInHand().getType() == Material.WATCH) {

				if (cellH.hasRingingCall(playerName)) {
					cellH.setUpCall(playerName, cellH.getRinger(playerName));
					CellRinger cellR = callingCalls.get(playerName);
					cellR.stop(0);
					callingCalls.remove(playerName);
					return;
				}

				if (ply.isSneaking()) {
					if (cellH.checkForCall(playerName)) {
						cellH.hangUp(playerName);
						return;
					}
				}

				if (usingCell.contains(ply.getName()))
					return;

				ply.sendMessage(ChatColor.GREEN + "Enter the players name you wish to call");
				usingCell.add(playerName);

			}

			if (ply.getItemInHand().getType() == Material.COMPASS) {

				if (ply.isSneaking()) {
					if (ply.getItemInHand().getType() == Material.COMPASS) {
						if (walkieM.leaveAllChannels(playerName)) {
							ply.sendMessage(ChatColor.GREEN + "Your walkie was turned off!");
							return;
						}
					}
				}

				plugin.getWalkieMenu().walkieChannelSelection(ply);
			}

		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent ev) {
		Player ply = ev.getPlayer();
		String playerName = ply.getName();

		walkieM.leaveAllChannels(playerName);

		if (cellH.checkForCall(playerName)) {
			cellH.hangUp(playerName);
		}
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent ev) {
		Player ply = ev.getEntity().getPlayer();
		if (ply == null)
			return;
		String playerName = ply.getName();

		walkieM.leaveAllChannels(playerName);

		if (cellH.checkForCall(playerName)) {
			cellH.hangUp(playerName);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent ev) {
		Player ply = (Player) ev.getWhoClicked();

		if (ply == null)
			return;

		String playerName = ply.getName();

		if (walkieM.channelCheck(playerName) && walkieM.subchannelCheck(playerName)) {
			if (!(isInHotBar(ply, Material.COMPASS))) {
				walkieM.leaveAllChannels(playerName);
				ply.sendMessage(ChatColor.GREEN + "Your walkie was turned off!");
			}
		}

		if (cellH.checkForCall(ply.getName())) {
			if (!(isInHotBar(ply, Material.WATCH))) {
				if (cellH.checkForCall(playerName)) {
					cellH.hangUp(playerName);
				}
			}
		}

	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent ev) {
		String esstenialsFormat = ev.getFormat();

		Player ply = ev.getPlayer();
		if (!(ply.hasPermission("advancedchat.chatcolour"))) {
			ev.setMessage(stripColour(ev.getMessage()));
		}
		String msgMarker1 = ev.getMessage().substring(0, 1);
		String msgMarker2 = ev.getMessage().substring(ev.getMessage().length() - 1, ev.getMessage().length());
		if (msgMarker1.equals("!")) {
			if (!this.disableGlobal.contains(ply.getName())) {
				int globalWait = plugin.getCfg().getInt("globalMsgWait");
				if (!(ply.hasPermission("advancedchat.globalwait"))) {
					if (lastGlobalMsg.containsKey(ply.getName())) {
						long lastSaid = lastGlobalMsg.get(ply.getName());
						if (!(((System.currentTimeMillis() / 1000) - lastSaid) > globalWait)) {
							ply.sendMessage("Please wait " + globalWait + " seconds between messages");
							ev.setCancelled(true);
							return;
						}
					}
				}

				List<String> recipients = new ArrayList<String>();
				if (!(ply.hasPermission("advancedchat.bypassglobal"))) {

					for (Player plys : ev.getRecipients()) {
						if (disableGlobal.contains(plys.getName()))
							continue;
						recipients.add(plys.getName());
					}

					ev.getRecipients().clear();

					for (String plys : recipients) {
						ev.getRecipients().add(Bukkit.getPlayer(plys));
					}

				}

				ev.setMessage(ev.getMessage().substring(1, ev.getMessage().length()));
				String formatedMessage = msgH.getMsgFormat(ply.getDisplayName(), ply.getName(), ev.getMessage(), "Global", esstenialsFormat);
				ev.setFormat(formatedMessage);
				lastGlobalMsg.put(ply.getName(), System.currentTimeMillis() / 1000);
				return;
			} else {
				ply.sendMessage(ChatColor.RED + "You have global chat disabled. Type /global on");
				ev.setCancelled(true);
				return;
			}
		}

		ev.setCancelled(true);
		ev.getRecipients().clear();

		if (cellH.checkForCall(ply.getName())) {

			int localRange = plugin.getLocalData().getLocalRange();
			double clearRange = localRange / 4.0;
			String message = ev.getMessage();

			if (msgMarker1.equals("(") && msgMarker2.equals(")")) {
				clearRange = plugin.getWhisperData().getWhisperRange();
				message = ev.getMessage().substring(1, ev.getMessage().length() - 1);
			}

			List<Entity> localE = ply.getNearbyEntities(clearRange, clearRange, clearRange);
			List<String> localRecipients = new ArrayList<String>();
			List<String> recipients = new ArrayList<String>();

			for (String p : cellH.getCellManager(ply.getName()).getCallParticipants()) {
				recipients.add(p);
			}

			for (Entity e : localE) {

				if (e instanceof Player) {
					Player p = Bukkit.getPlayer(((Player) e).getName());

					if (recipients.contains(p.getName()))
						continue;

					localRecipients.add(p.getName());
				}

			}

			sendSemiLocalMsg(ply, message, localRecipients, null);
			sendCellMsg(ply, message, recipients);
			return;
		}

		if (usingCell.contains(ply.getName())) {
			String[] targets = ev.getMessage().split(" ");
			String target = targets[0].trim();
			Player targetPlayer = Bukkit.getPlayer(target);

			if (targetPlayer == null || !targetPlayer.isOnline()) {
				ply.sendMessage(ChatColor.RED + "Either this player does not exist or is offline!");
				usingCell.remove(ply.getName());
				return;
			}
			String targetName = targetPlayer.getName();
			if (cellH.checkForCall(targetName)) {
				ply.sendMessage(ChatColor.RED + "It seems " + target + " is already in a call!");
				usingCell.remove(ply.getName());
				return;
			}
			if (!isInHotBar(targetPlayer, Material.WATCH)) {
				ply.sendMessage(ChatColor.RED + "They have not got a cell in there hotbar!");
				usingCell.remove(ply.getName());
				return;
			}
			if (cellH.isBlocked(targetName, ply.getName())) {
				ply.sendMessage(ChatColor.RED + "You have been blocked from calling " + targetName);
				usingCell.remove(ply.getName());
				return;
			}

			CellRinger cellR = new CellRinger(plugin);

			cellR.ringPlayer(targetName, ply.getName());
			callingCalls.put(targetName, cellR);
			ply.sendMessage(ChatColor.GREEN + "You have started to ring " + target);
			usingCell.remove(ply.getName());
			return;

		}

		if (isInHotBar(ply, Material.COMPASS)) {

			if (walkieM.channelCheck(ply.getName()) && walkieM.subchannelCheck(ply.getName())) {

				int localRange = plugin.getLocalData().getLocalRange();
				double clearRange = localRange / 4.0;
				String message = ev.getMessage();

				if (msgMarker1.equals("(") && msgMarker2.equals(")")) {
					clearRange = plugin.getWhisperData().getWhisperRange();
					message = ev.getMessage().substring(1, ev.getMessage().length() - 1);
				}

				List<Entity> localE = ply.getNearbyEntities(clearRange, clearRange, clearRange);
				List<String> localRecipients = new ArrayList<String>();
				List<String> recipients = new ArrayList<String>();

				int mainC = walkieM.getMainChannel(ply.getName());
				int secondC = walkieM.getSecondChannel(ply.getName());

				if (walkieM.doesMainChannelExist(mainC) == false)
					return;

				if (!(walkieM.getRecipients(mainC, secondC) == null)) {

					for (String p : walkieM.getRecipients(mainC, secondC)) {
						recipients.add(p);
					}

				} else {
					recipients.add(ply.getName());
				}

				for (Entity e : localE) {
					if (e instanceof Player) {
						Player p = Bukkit.getPlayer(((Player) e).getName());

						if (recipients.contains(p.getName()))
							continue;

						localRecipients.add(p.getName());
					}
				}

				sendSemiLocalMsg(ply, message, localRecipients, null);
				sendWalkieMsg(ply, message, recipients);
				return;

			}

		}

		if (msgMarker1.equals("(") && msgMarker2.equals(")")) {

			String message = ev.getMessage().substring(1, ev.getMessage().length() - 1);
			int whisperRange = plugin.getWhisperData().getWhisperRange();

			List<Entity> localE = ply.getNearbyEntities(whisperRange, whisperRange, whisperRange);
			List<String> localRecipients = new ArrayList<String>();

			for (Entity e : localE) {
				if (e instanceof Player) {
					Player p = Bukkit.getPlayer(((Player) e).getName());
					localRecipients.add(p.getName());
				}
			}
			localRecipients.add(ply.getName());

			sendWhisperMsg(ply, message, localRecipients);
			return;
		}

		ev.setMessage(stripColour(ev.getMessage()));
		int localRange = plugin.getLocalData().getLocalRange();
		double clearRange = localRange / 2.0;

		List<Entity> localE = ply.getNearbyEntities(localRange, localRange, localRange);
		List<String> localRecipients = new ArrayList<String>();
		List<String> cutOffRecipients = new ArrayList<String>();

		for (Entity e : localE) {
			if (e instanceof Player) {
				Player p = Bukkit.getPlayer(((Player) e).getName());
				double distance = ply.getLocation().distance(p.getLocation());
				if (p.getName() == ply.getName())
					continue;
				if (distance > clearRange) {
					cutOffRecipients.add(p.getName());
					continue;
				}
				localRecipients.add(p.getName());
			}
		}
		localRecipients.add(ply.getName());

		sendLocalMsg(ply, ev.getMessage(), localRecipients, cutOffRecipients);
	}

	public void sendLocalMsg(Player ply, String message, List<String> localRecipients, List<String> cutOffRecipients) {

		if (localRecipients.size() <= 1 && cutOffRecipients.isEmpty()) {
			ply.sendMessage(plugin.getLocalData().getunRecived(ply));
			return;
		}

		String formatedMessage = msgH.getMsgFormat(ply.getDisplayName(), ply.getName(), message, "Local", null);
		LocalChatData localCD = plugin.getLocalData();
		int localRange = localCD.getLocalRange();

		for (String p : localRecipients) {
			Player player = Bukkit.getPlayer(p);

			if (player.getName() == ply.getName()) {

				if (localCD.setSenderColour()) {
					player.sendMessage(changeSenderColor(player, formatedMessage, "Local"));
					continue;
				}

				player.sendMessage(formatedMessage);
				continue;
			}

			player.sendMessage(formatedMessage);
		}

		plugin.log.info(stripColour(formatedMessage));

		if (cutOffRecipients != null) {
			double clearRange = localRange / 2.0;

			for (String p : cutOffRecipients) {
				Player player = Bukkit.getPlayer(p);
				double distance = ply.getLocation().distance(player.getLocation());
				double noise = (distance - clearRange) / localRange;
				double clarity = 1 - noise;

				message = msgH.garbleMessage(message, clarity, "Local");
				formatedMessage = msgH.getMsgFormat(ply.getDisplayName(), ply.getName(), message, "Local", null);
				player.sendMessage(formatedMessage);
			}
		}
	}

	private void sendSemiLocalMsg(Player ply, String message, List<String> localRecipients, List<String> cutOffRecipients) {

		String formatedMessage = msgH.getMsgFormat(ply.getDisplayName(), ply.getName(), message, "Local", null);

		for (String p : localRecipients) {
			Player player = Bukkit.getPlayer(p);
			player.sendMessage(formatedMessage);
		}

	}

	public void sendWalkieMsg(Player ply, String message, List<String> recipients) {

		if (recipients.size() <= 1) {
			ply.sendMessage(plugin.getWalkieData().getunRecived(ply));
			return;
		}

		String formatedMessage = msgH.getMsgFormat(ply.getDisplayName(), ply.getName(), message, "Walkie", null);
		WalkieChatData walkieCD = plugin.getWalkieData();

		for (String p : recipients) {
			Player player = Bukkit.getPlayer(p);

			if (player.getName() == ply.getName()) {

				if (walkieCD.setSenderColour()) {
					player.sendMessage(changeSenderColor(player, formatedMessage, "Walkie"));
					continue;
				}

				player.sendMessage(formatedMessage);
				continue;
			}

			player.sendMessage(formatedMessage);
		}
	}

	public void sendWhisperMsg(Player ply, String message, List<String> recipients) {

		if (recipients.size() <= 1) {
			ply.sendMessage(plugin.getLocalData().getunRecived(ply));
			return;
		}

		String formatedMessage = msgH.getMsgFormat(ply.getDisplayName(), ply.getName(), message, "Whisper", null);
		WhisperChatData whisperCD = plugin.getWhisperData();

		for (String p : recipients) {
			Player player = Bukkit.getPlayer(p);

			if (player.getName() == ply.getName()) {

				if (whisperCD.setSenderColour()) {
					player.sendMessage(changeSenderColor(player, formatedMessage, "Whisper"));
					continue;
				}

				player.sendMessage(formatedMessage);
				continue;
			}
			player.sendMessage(formatedMessage);
		}
		plugin.log.info(stripColour(formatedMessage));
	}

	public void sendCellMsg(Player ply, String message, List<String> recipients) {

		String formatedMessage = msgH.getMsgFormat(ply.getDisplayName(), ply.getName(), message, "Cell", null);
		CellChatData cellCD = plugin.getCellData();

		for (String p : recipients) {
			Player player = Bukkit.getPlayer(p);

			if (player.getName() == ply.getName()) {

				if (cellCD.setSenderColour()) {
					player.sendMessage(changeSenderColor(player, formatedMessage, "Cell"));
					continue;
				}

				player.sendMessage(formatedMessage);
				continue;
			}

			player.sendMessage(formatedMessage);
		}

	}

	public void sendGlobalMsg(Player ply, String message, List<String> recipients) {
		for (String p : recipients) {
			Player player = Bukkit.getPlayer(p);
			player.sendMessage(msgH.getMsgFormat(ply.getDisplayName(), ply.getName(), message, "Global", null));
		}
	}

	private String changeSenderColor(Player player, String formatedMessage, String type) {

		switch (type) {
		case "Local":
			LocalChatData localCD = plugin.getLocalData();
			formatedMessage = formatedMessage.replaceFirst(player.getDisplayName(), localCD.getSenderColour() + player.getDisplayName());
			return msgH.setColour(formatedMessage);
		case "Walkie":
			WalkieChatData walkieCD = plugin.getWalkieData();
			formatedMessage = formatedMessage.replaceFirst(player.getDisplayName(), walkieCD.getSenderColour() + player.getDisplayName());
			return msgH.setColour(formatedMessage);
		case "Cell":
			CellChatData cellCD = plugin.getCellData();
			formatedMessage = formatedMessage.replaceFirst(player.getDisplayName(), cellCD.getSenderColour() + player.getDisplayName());
			return msgH.setColour(formatedMessage);

		default:
			return formatedMessage;
		}

	}

	/*
	 * private String addEssentialsFormat(String oldFormat, String newFormat,
	 * Player ply) { newFormat = newFormat.replace("[Essentials-format]",
	 * oldFormat); newFormat = newFormat.replace(ply.getDisplayName(),
	 * oldFormat); return newFormat; }
	 */

	public String stripColour(String cMessage) {
		cMessage = stripColourSymbol(cMessage);
		cMessage = cMessage.replace("[Black]", "");
		cMessage = cMessage.replace("[Dark_Blue]", "");
		cMessage = cMessage.replace("[Dark_Green]", "");
		cMessage = cMessage.replace("[Dark_Aqua]", "");
		cMessage = cMessage.replace("[Dark_Red]", "");
		cMessage = cMessage.replace("[Dark_Purple]", "");
		cMessage = cMessage.replace("[Dark_Grey]", "");
		cMessage = cMessage.replace("[Gold]", "");
		cMessage = cMessage.replace("[Gray]", "");
		cMessage = cMessage.replace("[Blue]", "");
		cMessage = cMessage.replace("[Green]", "");
		cMessage = cMessage.replace("[Aqua]", "");
		cMessage = cMessage.replace("[Red]", "");
		cMessage = cMessage.replace("[Yellow]", "");
		cMessage = cMessage.replace("[White]", "");
		cMessage = cMessage.replace("[Light_Purple]", "");
		return cMessage;
	}

	public String stripColourSymbol(String cMessage) {
		return cMessage.replaceAll("(?i)\u00A7[0-9A-VK]", " ");
	}

	public boolean isInHotBar(Player ply, Material mat) {
		Inventory inv = ply.getInventory();
		for (int i = 0; i < 9; i++) {
			ItemStack item = inv.getItem(i);
			if (item != null) {
				if (item.getType() == mat) {
					return true;
				} else {
					continue;
				}
			}
		}
		return false;
	}

}
