AdvancedChat
This plugin a that changes the way minecraft chat is handled using 4 new types of chat Local, Global, Cell and Walkie Talkie. Each have there own ways of functioning and formats.

All the new types of chat can have there own formats set changing either colour the way the message, name, prefix, suffix and Chat Type display.
There order can be arranged to sort your personal preference.

Local: This allows local chatting with a certain range(Set in config) anything further will start to drop out and become unreadable to the point of a hard limit which the chat drops
completely this is great for large servers. 

Global: This allows the player to talk to everyone on the server no matter there location. There are no restrictions to this it is almost identical to default mc chat.

Cell: People can use a watch as a cell phone and ring a certain player as long as they have a cell in there hotbar. This allows players to communicate far distances and not display
on global chat or having to use the private messaging service. 

Walkie Talkie: This has a 81 Channels total which players can join and talk to other player again without talking to everyone on the server. This also has no range and can be used
anywhere on the map.

